import json
import logging

import mcai_worker_sdk as mcai

from inaSpeechSegmenter import Segmenter


class McaiWorkerParameters(mcai.WorkerParameters):
    source_path: str
    destination_path: str


class McaiWorker(mcai.Worker):
    segmenter = None

    def setup(self) -> None:
        self.segmenter = Segmenter()

    def process(
        self, channel: mcai.McaiChannel, parameters: McaiWorkerParameters, _job_id: int
    ):
        logging.info("Start audio segmentation.")
        segmentation = self.segmenter(parameters.source_path)

        with open(parameters.destination_path, "w", encoding="utf-8") as file:
            file.write(json.dumps(segmentation, indent=2))

        channel.set_job_status(mcai.JobStatus.Completed)


def main():
    description = mcai.WorkerDescription(__package__)
    worker = McaiWorker(McaiWorkerParameters, description)
    worker.start()
