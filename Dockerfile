FROM python:3.9-buster

COPY pyproject.toml /src/
ADD py_speech_segmentation_worker /src/py_speech_segmentation_worker
WORKDIR /src

RUN apt-get update && \
    apt-get install ffmpeg -y

RUN pip install .

ENV AMQP_QUEUE=job_speech_segmentation
ENV MCAI_LOG=info

CMD py_speech_segmentation_worker
